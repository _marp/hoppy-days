extends AnimatedSprite

func update(motion):
	if motion.x < 0:
		play("run")
		flip_h = true
	if motion.x > 0:
		play("run")
		flip_h = false
	if motion.x == 0:
		play("default")
		flip_h = false
	if motion.y < 0:
		play("jump")
		flip_h = false
