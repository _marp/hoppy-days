extends Node2D

export var starting_lives = 3
var coins_target = 5

var lives
var coins

func _ready():
	Global.GameState = self
	lives = starting_lives
	coins = 0
	update_GUI()

func update_GUI():
	Global.GUI.update_GUI(lives, coins)

func hurt():
	lives -= 1
	if lives < 0:
		end_game()
	update_GUI()
	Global.GUI.bunny_hurt()

func coin_up():
	coins += 1
	update_GUI()
	Global.GUI.coin_up()
	if (coins % coins_target) == 0:
		life_up()

func life_up():
	lives += 1
	update_GUI()
	Global.GUI.life_up()

func end_game():
	get_tree().change_scene(Global.GameOver)

func _on_Portal_body_entered(body):
	get_tree().change_scene(Global.Victory)
