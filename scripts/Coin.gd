extends AnimatedSprite

var taken

func _ready():
	taken = false

func _on_RigidBody2D_body_entered(body):
	if body == Global.Player and not taken:
		Global.GameState.coin_up()
		$AnimationPlayer.play("die")
		$AudioStreamPlayer.play()
		taken = true

func die():
	queue_free()
