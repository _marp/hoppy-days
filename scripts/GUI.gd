extends CanvasLayer

func _ready():
	Global.GUI = self

func update_GUI(lives, coins):
	$GUI/HBoxContainer/LifeLabel.text = str(lives)
	$GUI/HBoxContainer/CoinLabel.text = str(coins)

func bunny_hurt():
	$GUI/AnimationPlayer.play("BunnyHurt")

func life_up():
	$GUI/AnimationPlayer.play("LifePulse")

func coin_up():
	$GUI/AnimationPlayer.play("CoinPulse")