extends Area2D

const SPEED = 500
var xpos = Vector2()

func _ready():
	xpos.x = global_position.x

func _physics_process(delta):
	move_down_screen(delta)
	manage_collision()
	
	if not $VisibilityNotifier2D.is_on_screen():
		queue_free()

func move_down_screen(delta):
	global_position.y += SPEED * delta
	global_position.x = xpos.x

func manage_collision():
	var collider = get_overlapping_bodies()
	for bodie in collider:
		if bodie == Global.Player:
			Global.Player.hurt()
			Global.GameState.hurt()
		queue_free()
